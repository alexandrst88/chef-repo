#
#
#
#


package 'nginx' do
  action :install
end

service 'nginx' do 
  action [ :enable, :start]
end

template '/etc/nginx/sites-available/websockify' do
  source 'websockify.erb'
  notifies :restart, 'service[nginx]', :immediately
end

link '/etc/nginx/sites-enabled/websockify' do
  to '/etc/nginx/sites-available/websockify'
end
