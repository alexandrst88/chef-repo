#
# Cookbook Name:: pypi-server
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "pypi-server::nginx"
include_recipe "pypi-server::python_packages"

execute 'run_pypi' do
  command 'gunicorn -w4 -b :8000 \'pypiserver:app("/tmp")\' &'
  action :run
end
