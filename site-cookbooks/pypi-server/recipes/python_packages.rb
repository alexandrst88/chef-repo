# There, will include all python package which need to run pypi-server


package 'python-pip' do
  action :install
end

for package in [ "gunicorn", "pypiserver" ] do
  python_pip package do
    action [:install]
  end
end


